resource "vault_mount" "transit" {
  path = "transit/app1"
  type = "transit"

  options = {
    convergent_encryption = false
  }
}

resource "vault_transit_secret_backend_key" "key" {
  backend = vault_mount.transit.path
  name    = "first_key"
}
