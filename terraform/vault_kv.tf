resource "vault_mount" "kv" {
  type = "kv-v2"
  path = "kv/app1"
}

resource "vault_generic_secret" "dev" {
  path = "${vault_mount.kv.path}/dev/envs"

  data_json = <<EOT
{
  "DATABASE_HOST": "postgres",
  "DATABASE_USER":   "karel",
  "DATABASE_PASSWORD": "quooJeiyo6oop5ohThei"
}
EOT
}

resource "vault_generic_secret" "prod" {
  path = "${vault_mount.kv.path}/prod/envs"

  data_json = <<EOT
{
  "DATABASE_HOST": "postgres",
  "DATABASE_USER": "karel",
  "DATABASE_PASSWORD": "po3po5cheeH1coCoV2xa"
}
EOT
}
