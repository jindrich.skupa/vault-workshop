terraform {
  #  backend "http" {  }
}

provider "gitlab" {
  token = var.gitlab_token
}

provider "vault" {
  address = var.vault_url
  auth_login {
    path = "auth/approle/login"
    parameters = {
      role_id   = var.login_approle_role_id
      secret_id = var.login_approle_secret_id
    }
  }
}

provider "postgresql" {
  host            = "localhost"
  port            = "5432"
  superuser       = false
  database        = "postgres"
  username        = var.postgres_user
  password        = var.postgres_password
  sslmode         = "disable"
  connect_timeout = 15
}
