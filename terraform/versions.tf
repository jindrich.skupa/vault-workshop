terraform {
  required_version = ">= 1.0, < 2.0"

  required_providers {
    vault = {
      source  = "hashicorp/vault"
      version = "3.6.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.15.0"
    }
    postgresql = {
      source  = "cyrilgdn/postgresql"
      version = "1.16.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.3.1"
    }
  }
}
