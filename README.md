# Hashicorp Vault

Support repos:

* https://gitlab.com/jindrich.skupa/vault-workshop-infra
* https://gitlab.com/jindrich.skupa/vault-workshop-pipeline

## Virtual lab (cloud)

```
wget https://gitlab.com/jindrich.skupa/vault-workshop/-/raw/main/lab/access/id_rsa_vault -O id_rsa_vault
chmod 600 id_rsa_vault
ssh -i id_rsa_vault vault@vault-{a,b,c,d,e,f}-2022.installfest.sgfl.xyz

# on server or localy
git clone https://gitlab.com/jindrich.skupa/vault-workshop.git
```

## Vault lab start

* start as docker compose with zero config

```bash
cd lab
docker-compose up
open http://localhost:8200
```

* save **Unseal key** and **Root token**
* run new terminal

```bash
export VAULT_TOKEN="s.something"
export VAULT_ADDR="http://localhost:8200"
apt add jq curl openssh-client postgresql-client
```

## Key/Value storage

```bash
vault secrets enable -path=kv2/app1 kv-v2
vault kv put kv2/app1/dev/envs foo=bar bar=foo
vault kv get kv2/app1/dev/envs
export VAULT_FORMAT=json
vault kv patch kv2/app1/dev/envs key=value
export VAULT_FORMAT=table
vault kv delete kv2/app1/dev/envs
vault kv undelete -versions=2 secret/my-secret
vault kv destroy -versions=2 kv2/app1/dev/envs
vault kv get -version=1 kv2/app1/dev/envs
vault kv metadata get kv2/app1/dev/envs
vault kv metadata put -max-versions 2 -delete-version-after="3h25m19s" kv2/app1/dev/envs
```

## Key/Value storage (curl)

```bash
# v2
curl -s -XGET -H "X-Vault-Token: $VAULT_TOKEN" \
  $VAULT_ADDR/v1/kv2/app1/data/dev/envs | \
  jq .data.data

# v1
curl -s -XGET -H "X-Vault-Token: $VAULT_TOKEN" \
  $VAULT_ADDR/v1/kv2/app1/dev/envs | \
  jq .data.data
```

## Transit

```bash
vault secrets enable transit
vault write -f transit/keys/mysecret

vault write transit/encrypt/mysecret \
  plaintext=$(echo -n "Hack the planet!" | \
    base64 -w 0)

export VAULT_FORMAT=json

vault write transit/decrypt/mysecret \
  "ciphertext=vault:v1:IVoENPtsao6SKdKTHZRgzSQQBCXNR7GomRIbm3d+1ViaC5WI15pVFJc5GxU=" | \
  jq .data.plaintext | \
  tr -d \"\" | base64 -d

unset VAULT_FORMAT
```

## SSH

no more authorized_keys, use ssh CA signed keys

```bash
vault secrets enable -path=ssh-signer ssh
vault write ssh-signer/config/ca generate_signing_key=true

curl -o /etc/ssh/ca.pub \
  http://vault:8200/v1/ssh-signer/public_key
```

`/etc/ssh/sshd_config`

```
TrustedUserCAKeys /etc/ssh/ca.pub
```



### signer role

```bash
vault write ssh-signer/roles/admin -<<EOH
{
  "algorithm_signer": "rsa-sha2-256",
  "allow_user_certificates": true,
  "allowed_users": "*",
  "allowed_extensions": "permit-pty,permit-port-forwarding",
  "default_extensions": [ { "permit-pty": "" } ],
  "key_type": "ca",
  "default_user": "admin",
  "ttl": "30m0s"
}
EOH
```


### sign and use the key

```bash
ssh-keygen -t rsa -f $HOME/.ssh/id_rsa \
  -C "vault@example.com"

vault write -field=signed_key ssh-signer/sign/admin \
    public_key=@$HOME/.ssh/id_rsa.pub > signed_cert.pub

ssh -i signed_cert.pub -i ~/.ssh/id_rsa admin@ssh
```

## PostgreSQL

```bash
vault secrets enable database

vault write database/config/postgresql \
    plugin_name=postgresql-database-plugin \
    allowed_roles="pgrole" \
    connection_url="postgresql://{{username}}:{{password}}@postgres:5432/postgres?sslmode=disable" \
    username="postgres" \
    password="postgres"

vault write database/roles/pgrole \
    db_name=postgresql \
    creation_statements="CREATE ROLE \"{{name}}\" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}'; \
        GRANT SELECT ON ALL TABLES IN SCHEMA public TO \"{{name}}\";" \
    default_ttl="1h" \
    max_ttl="24h"
```

### read temporary user credentials

```bash
vault read database/creds/pgrole
```

```json
{
  "request_id": "88d270f4-6aba-f6cd-7c14-3267fd77960b",
  "lease_id": "database/creds/pgrole/AOjEuPeaOP2rc1QBL5Yiowfg",
  "lease_duration": 3600,
  "renewable": true,
  "data": {
    "password": "h3o1hy-aJSR0xKI77cFJ",
    "username": "v-root-pgrole-YeqQe755Kx3jwtJq3iuL-1645704090"
  },
  "warnings": null
}
```

```bash
psql postgres://v-root-pgrole-VO3deIOrw7b7c3uSAq8c-1654871376:t6rTE-lJ8o-NBHMCX0hK@postgres:5432
```

## PKI

### Root CA

```bash
vault secrets enable pki
# 10 years
vault secrets tune -max-lease-ttl=87600h pki

vault write -field=certificate pki/root/generate/internal \
  common_name="example.com" \
  ttl=87600h > CA_cert.crt

vault write pki/config/urls \
  issuing_certificates="$VAULT_ADDR/v1/pki/ca" \
  crl_distribution_points="$VAULT_ADDR/v1/pki/crl"
```

### Intermediate CA

```bash
vault secrets enable -path=pki_int pki
# 5 years
vault secrets tune -max-lease-ttl=43800h pki_int

vault write -format=json pki_int/intermediate/generate/internal \
  common_name="example.com Intermediate Authority" \
  | jq -r '.data.csr' > pki_intermediate.csr

vault write -format=json pki/root/sign-intermediate csr=@pki_intermediate.csr \
  format=pem_bundle ttl="43800h" \
  | jq -r '.data.certificate' > intermediate.cert.pem

vault write pki_int/intermediate/set-signed certificate=@intermediate.cert.pem
```

### Certificates

```
vault write pki_int/roles/example-dot-com \
     allowed_domains="example.com" \
     allow_subdomains=true \
     max_ttl="720h"

vault write pki_int/issue/example-dot-com common_name="test.example.com" ttl="24h"

vault list pki_int/certs
vault read pki_int/cert/42:3b:8d:35:2c:22:5f:f9:d9:1a:31:67:a3:35:16:b3:c3:de:4d:ae
```

## Policies

* path + operations

```bash
cat << EOF > dev-ro.policy
path "kv2/prj/dev/*" {
    capabilities = ["read", "list"]
}
EOF

vault policy write dev-ro dev-ro.policy
vault policy list
```

### Gitlab OIDC

```bash
# user authentication
vault auth enable oidc
vault write auth/oidc/config \
  bound_issuer=gitlab.com \
  oidc_discovery_url="https://gitlab.com" \
  oidc_client_id=0cde3438-f562-467a \
  oidc_client_secret=abe8-651dc4823148 \
  bound_issuer="localhost"
```

```bash
vault write auth/oidc/role/admin - <<EOF
{
   "user_claim":"nickname",
   "allowed_redirect_uris": [
     "http://localhost:8250/oidc/callback",
     "http://localhost:8200/ui/vault/auth/oidc/oidc/callback",
     "https://vault-2022.installfest.sgfl.xyz/oidc/callback",
     "https://vault-2022.installfest.sgfl.xyz/ui/vault/auth/oidc/oidc/callback"
   ],
   "bound_audiences": "542ed84b2b0236475a21dcf148b3195fbb3eb354b69b8fca8d72a2d96e2648cc",
   "oidc_scopes": "openid",
   "role_type": "oidc",
   "policies": ["admins"],
   "ttl": "1h",
   "bound_claims": { "groups": ["vault-ar8ac"] }
}
EOF
```

```bash
vault policy write admins -<<EOF
path "*" { 
  capabilities = ["sudo","read","create","update","delete","list", "patch"]
}
EOF
```


### Gitlab JWT /  pipeline

```bash
# pipelines authentication
vault auth enable -path=gitlab-jwt jwt 
vault write auth/gitlab-jwt/config \
  bound_issuer=gitlab.com \
  jwks_url="https://gitlab.com/-/jwks"

```

```bash
vault write auth/gitlab-jwt/role/app1-pipeline - <<EOF
{
  "role_type": "jwt",
  "policies": ["app1-pipeline"],
  "token_explicit_max_ttl": 60,
  "user_claim": "user_email",
  "bound_claims_type": "glob",
  "bound_claims": {
    "project_id": "36930843"
  }
}
EOF
```

```bash
vault policy write app1-pipeline - <<EOF
# Read-only permission on 'kv/app1/data/dev/*' path

path "kv/app1/data/dev/*" {
  capabilities = [ "read" ]
}
EOF
```

```
export VAULT_TOKEN="$(vault write \
-field=token auth/gitlab-jwt/login \
  role=pipeline-prj \
  jwt=${CI_JOB_JWT})"

vault kv get -format json kv2/prj/pipeline/secrets/envs\
  | jq -r '.data.data | to_entries[] | "\(.key)=\(.value)"' \
  > .env
```

```yaml
image: vault:1.10.3

before_script:
  - apk add jq curl
  - export VAULT_ADDR="https://vault-2022.installfest.sgfl.xyz"
  - export VAULT_TOKEN="$(vault write -field=token auth/gitlab-jwt/login role=app1-pipeline jwt=${CI_JOB_JWT})"
  - vault kv get -format json kv/app1/dev/envs | jq -r '.data.data | to_entries[] | "\(.key)=\(.value)"' > .env
```

## Terraform

```bash
vault auth enable approle
```

# create terraform approle

```bash
vault write auth/approle/role/terraform \
    token_num_uses=0 \
    token_ttl=20m \
    token_max_ttl=30m
```

# read credentials

```bash
vault read auth/approle/role/terraform/role-id
# Key        Value
# ---        -----
# role_id    71674a5b-d988-7513-c6a3-88a7f26914f6

vault write -f auth/approle/role/terraform/secret-id
# Key                   Value
# ---                   -----
# secret_id             124a7e5e-2151-c8a1-0dcf-7c9d00c98b2a
# secret_id_accessor    83ea670d-4a11-d359-7119-75d3ddf21cf8
# secret_id_ttl         0s
```

```
vault policy write terraform -<<EOF
path "*" { capabilities = ["sudo","read","create","update","delete","list", "patch"] }
EOF
```

```bash
vault write auth/approle/role/terraform token_policies="terraform" \
    token_ttl=1h token_max_ttl=4h
```


